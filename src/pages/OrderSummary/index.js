import {StyleSheet, Text, View, ScrollView} from 'react-native';
import React from 'react';
import {Button, Headers, ItemListFood, ItemValue} from '../../components';
import {FoodDummy4, IcBackWhite} from '../../assets';

const OrderSummary = ({navigation}) => {
  return (
    <ScrollView>
      <Headers
        title="Order Summary"
        subTitle="Order Detail"
        onBack={() => {}}
      />
      <View style={styles.content}>
        <Text style={styles.label}>Item Ordered</Text>
        <ItemListFood
          image={FoodDummy4}
          type="order-summary"
          name="Sop Iga"
          price="34.000"
          items={14}
        />
        <Text style={styles.label}>Details Transaction</Text>
        <ItemValue label="Soup Bumil" value="IDR. 34.000" />
        <ItemValue label="Driver" value="IDR. 8.000" />
        <ItemValue label="Tax 10%" value="IDR. 3.400" valueColor="red" />
        <ItemValue
          label="Total Price"
          value="IDR. 45.400"
          valueColor="#1ABC9C"
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Delivery To :</Text>
        <ItemValue label="Name" value="Egi Permana" />
        <ItemValue label="Address" value="Cikutra" />
        <ItemValue label="Phone No" value="082321596683" />
        <ItemValue label="House No" value="11" />
        <ItemValue label="City" value="Bandung" />
      </View>
      <View style={styles.button}>
        <Button
          title="Checkout Now"
          onPress={() => navigation.replace('SuccessOrder')}
        />
      </View>
    </ScrollView>
  );
};

export default OrderSummary;

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 16,
    marginTop: 24,
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    marginBottom: 8,
  },
  button: {paddingHorizontal: 24, marginTop: 24},
});
