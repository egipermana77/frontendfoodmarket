import ProfileDummy from './profile-dummy.png';
import FoodDummy1 from './food-dummy1.png';
import FoodDummy2 from './food-dummy2.png';
import FoodDummy3 from './food-dummy3.png';
import FoodDummy4 from './food-dummy4.png';
import FoodDummy6 from './food-dummy6.png';
import notif from './001-notification.png';
import home from './002-home.png';
import setting from './003-setting.png';
import logout from './004-logout.png';
import close from './close.png';
import menu from './menu.png';
import profile from './profile.jpg';
import logopaypal from './logopaypal.png';
import more from './more.png';
import nearby from './nearby.png';
import topup from './topup.png';
import wallet from './wallet.png';
import catfod1 from './catfod-1.png';
import catfod2 from './catfod-2.png';
import catfod3 from './catfod-3.png';
import catfod4 from './catfod-4.png';
import catfod5 from './catfod-5.png';
import catfod6 from './catfod-6.png';
import catfod7 from './catfod-7.png';
import catfod8 from './catfod-8.png';

export {
  ProfileDummy,
  FoodDummy1,
  FoodDummy2,
  FoodDummy3,
  FoodDummy4,
  FoodDummy6,
  notif,
  home,
  setting,
  logout,
  menu,
  close,
  profile,
  logopaypal,
  more,
  nearby,
  topup,
  wallet,
  catfod1,
  catfod2,
  catfod3,
  catfod4,
  catfod5,
  catfod6,
  catfod7,
  catfod8,
};
