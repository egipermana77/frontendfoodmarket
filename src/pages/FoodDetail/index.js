import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {FoodDummy6, IcBackWhite} from '../../assets';
import {Button, Counter, Rating} from '../../components';

const FoodDetail = ({navigation}) => {
  return (
    <View style={styles.page}>
      <ImageBackground source={FoodDummy6} style={styles.cover}>
        <TouchableOpacity
          style={styles.back}
          activeOpacity={0.7}
          onPress={() => {
            navigation.goBack();
          }}>
          <IcBackWhite />
        </TouchableOpacity>
      </ImageBackground>
      <View style={styles.content}>
        <View style={styles.mainContent}>
          <View style={styles.fooCounter}>
            <View>
              <Text style={styles.title}>Cherry Healthy</Text>
              <Rating />
            </View>
            <Counter />
          </View>
          <Text style={styles.desc}>
            Drumstick ball tip swine, ribeye beef ribs ham ham hock meatloaf
            jerky corned beef prosciutto shank pork chop. Pastrami pork loin
            corned beef spare ribs, beef ribs brisket venison jowl.
          </Text>
          <Text style={styles.label}>Integrients : </Text>
          <Text style={styles.desc}>Seledri, Selada, Jamur, Garam, Gula</Text>
        </View>
        <View style={styles.footer}>
          <View style={styles.priceContainer}>
            <Text style={styles.labelPrice}>Total Price</Text>
            <Text style={styles.totalPrice}>IDR. 24.000</Text>
          </View>
          <View style={styles.button}>
            <Button
              title="Order Now"
              onPress={() => navigation.navigate('OrderSummary')}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default FoodDetail;

const styles = StyleSheet.create({
  page: {flex: 1},
  cover: {height: 330, paddingTop: 26, paddingLeft: 24},
  back: {width: 30, height: 30, justifyContent: 'center', alignItems: 'center'},
  content: {
    backgroundColor: 'white',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    marginTop: -40,
    paddingTop: 26,
    paddingHorizontal: 16,
    flex: 1,
  },
  mainContent: {
    flex: 1,
  },
  fooCounter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 14,
  },
  title: {fontSize: 16, fontFamily: 'Poppins-Regular', color: '#020202'},
  desc: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
    marginBottom: 16,
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    marginBottom: 4,
  },
  footer: {flexDirection: 'row', paddingVertical: 16, alignItems: 'center'},
  priceContainer: {flex: 1},
  button: {width: 163},
  labelPrice: {fontSize: 13, fontFamily: 'Poppins-Regular', color: '#8D92A3'},
  totalPrice: {fontSize: 18, fontFamily: 'Poppins-Regular', color: '#020202'},
});
