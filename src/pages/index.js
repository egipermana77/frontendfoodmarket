import SplashScreen from './SplashScreen';
import SignIn from './Signin';
import SignUp from './SignUp';
import SignupAddress from './SignupAddress';
import SuccessSignUp from './SuccessSignUp';
import Home from './Home';
import Order from './Order';
import Profile from './Profile';
import FoodDetail from './FoodDetail';
import OrderSummary from './OrderSummary';
import SuccessOrder from './SuccessOrder';
import OrderDetail from './OrderDetail';

export {
  SplashScreen,
  SignIn,
  SignUp,
  SignupAddress,
  SuccessSignUp,
  Home,
  Order,
  Profile,
  FoodDetail,
  OrderSummary,
  SuccessOrder,
  OrderDetail,
};
