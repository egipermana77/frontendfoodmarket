import Axios from 'axios';
import {showToast, storeData} from '../../utils';
import {setLoading} from './global';

const API_HOST = {
  url: 'http://foodmarket-backend.buildwithangga.id/api',
};

export const signUpAction =
  (dataRegister, photoReducer, navigation) => dispatch => {
    Axios.post(`${API_HOST.url}/register`, dataRegister)
      .then(res => {
        // console.log('success :', res.data);

        //set store data user di local storage
        const profile = res.data.data.user;

        //set store data token di local storage
        const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
        storeData('token', {value: token});

        if (photoReducer.isUploadPhoto) {
          const photoForUpload = new FormData();
          photoForUpload.append('file', photoReducer);

          //handle upload photo
          Axios.post(`${API_HOST.url}/user/photo`, photoForUpload, {
            headers: {
              Authorization: token,
              'Content-Type': 'multipart/form-data',
            },
          })
            .then(resUpload => {
              profile.profile_photo_url = `http://foodmarket-backend.buildwithangga.id/storage/${resUpload.data.data[0]}`;
              storeData('userProfile', profile);
              navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]}); //reset agar berpindah halaman dan tidak bisa balik lagi
            })
            .catch(err => {
              console.log(err);
              showToast('Upload photo gagal');
              navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]}); //reset agar berpindah halaman dan tidak bisa balik lagi
            });
        } else {
          storeData('userProfile', profile);
          navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]}); //reset agar berpindah halaman dan tidak bisa balik lagi
        }

        dispatch(setLoading(false));
        showToast('Register success', 'success');
      })
      .catch(err => {
        // console.log('sign up error :', err.response.data.message);
        dispatch(setLoading(false));
        showToast(err?.response?.data?.message); //tanda ? maksudnya chaining if
      });
  };

export const sigInAction = (dataForm, navigation) => dispatch => {
  dispatch(setLoading(true));
  Axios.post(`${API_HOST.url}/login`, dataForm)
    .then(res => {
      const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
      const profile = res.data.data.user;

      dispatch(setLoading(false));
      storeData('token', {value: token});
      storeData('userProfile', profile);

      navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
    })
    .catch(err => {
      dispatch(setLoading(false));
      showToast(err?.response?.data?.message);
    });
};
