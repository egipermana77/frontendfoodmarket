import React from 'react';
import {ScrollView, StyleSheet, View, Text, Image} from 'react-native';
import {
  catfod1,
  catfod2,
  catfod3,
  catfod4,
  catfod5,
  catfod6,
  catfod7,
  catfod8,
  FoodDummy1,
  FoodDummy2,
  FoodDummy3,
  FoodDummy4,
  logopaypal,
  more,
  nearby,
  topup,
  wallet,
} from '../../assets';
import {FoodCard, Gap, HomeProfile, HomeTabSection} from '../../components';

const Home = () => {
  return (
    <ScrollView showsVerticalScrollIndicator={false} style={{flexGrow: 1}}>
      <View style={styles.page}>
        <HomeProfile />
        {
          //gopay
        }

        <View style={{paddingHorizontal: 20, marginBottom: 12}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: '#FFC700',
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
              padding: 14,
              alignItems: 'center',
            }}>
            <Image source={logopaypal} style={{width: 80, height: 20}} />
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#D9435E'}}>
              Rp.58.000
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 20,
              paddingBottom: 14,
              backgroundColor: '#facf37',
              borderBottomRightRadius: 5,
              borderBottomLeftRadius: 5,
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Image source={wallet} style={{tintColor: '#D9435E'}} />
              <Text
                style={{fontSize: 13, fontWeight: 'bold', color: '#D9435E'}}>
                Pay
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
              }}>
              <Image source={nearby} style={{tintColor: '#D9435E'}} />
              <Text
                style={{fontSize: 13, fontWeight: 'bold', color: '#D9435E'}}>
                Nearby
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
              }}>
              <Image source={topup} style={{tintColor: '#D9435E'}} />
              <Text
                style={{fontSize: 13, fontWeight: 'bold', color: '#D9435E'}}>
                Topup
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
              }}>
              <Image source={more} style={{tintColor: '#D9435E'}} />
              <Text
                style={{fontSize: 13, fontWeight: 'bold', color: '#D9435E'}}>
                more
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginHorizontal: 12,
            marginTop: 18,
            marginBottom: 24,
          }}>
          {/* category food */}
          <View
            style={{
              width: '100%',
              marginBottom: 12,
              paddingHorizontal: 8,
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                color: 'black',
                textAlign: 'left',
              }}>
              Category Food
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              marginBottom: 16,
            }}>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod1}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Tapioca
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod2}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Rice Vege
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod3}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Eat Well
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod4}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Hydroponic
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod5}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Vegetable
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod6}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Junk Food
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod7}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Meat Vege
              </Text>
            </View>
            <View
              style={{
                width: `${100 / 4}%`,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 62,
                  height: 62,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={catfod8}
                  style={{width: 50, height: 50, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  marginTop: 6,
                }}>
                Hotplate
              </Text>
            </View>
          </View>
        </View>

        <View>
          <Text
            style={{
              paddingHorizontal: 20,
              fontSize: 16,
              fontWeight: 'bold',
              color: 'black',
            }}>
            Our Choice For You
          </Text>
          <View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View style={styles.foodCardContainer}>
                <Gap width={24} />
                <FoodCard image={FoodDummy1} />
                <FoodCard image={FoodDummy2} />
                <FoodCard image={FoodDummy3} />
                <FoodCard image={FoodDummy4} />
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.tabContainer}>
          <HomeTabSection />
        </View>
        <View>
          <Text>Pembatas</Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  foodCardContainer: {flexDirection: 'row', marginVertical: 24},
  tabContainer: {flex: 1, height: 500},
});
