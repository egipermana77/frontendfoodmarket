import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {
  Animated,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  ProfileDummy,
  home,
  notif,
  setting,
  logout,
  menu,
  close,
  profile,
} from '../../assets';
import {ProfileTabSection} from '../../components';

const Profile = () => {
  const [currentTab, setCurrentTab] = useState('Home');

  // untuk set menu dan close
  const [showMenu, setShowMenu] = useState(false);

  //untuk animasi proferties
  const offsetValue = useRef(new Animated.Value(0)).current;

  const scaleValue = useRef(new Animated.Value(1)).current;
  const closeButtonOffset = useRef(new Animated.Value(0)).current;

  const navigation = useNavigation();

  //untuk handle sign out
  const SignOut = () => {
    AsyncStorage.multiRemove(['userProfile', 'token']).then(res => {
      navigation.reset({index: 0, routes: [{name: 'SignIn'}]});
    });
  };

  //for multiple button
  const tabButton = (currentTab, setCurrentTab, title, image) => {
    return (
      <TouchableOpacity
        onPress={() => {
          if (title === 'LogOut') {
            SignOut();
          } else {
            setCurrentTab(title);
          }
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingVertical: 8,
            backgroundColor: currentTab == title ? 'white' : 'transparent',
            paddingLeft: 20,
            paddingRight: 30,
            borderRadius: 8,
            marginTop: 15,
          }}>
          <Image
            source={image}
            style={{
              width: 25,
              height: 25,
              tintColor: currentTab == title ? '#D9435E' : 'white',
            }}
          />
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              paddingLeft: 15,
              color: currentTab == title ? '#D9435E' : 'white',
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.page}>
      <View style={{justifyContent: 'flex-start', padding: 15}}>
        <Image
          source={ProfileDummy}
          style={{
            width: 60,
            height: 60,
            borderRadius: 10,
            marginTop: 8,
          }}
        />
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'white',
            marginTop: 20,
          }}>
          Jesicca Jane
        </Text>
        <TouchableOpacity>
          <Text style={{color: 'white', marginTop: 6}}>View my profile</Text>
        </TouchableOpacity>

        <View style={{flexGrow: 1, marginTop: 20}}>
          {
            //tab bar button
          }
          {tabButton(currentTab, setCurrentTab, 'Home', home)}
          {tabButton(currentTab, setCurrentTab, 'Notification', notif)}
          {tabButton(currentTab, setCurrentTab, 'Setting', setting)}
        </View>
        <View>{tabButton(currentTab, setCurrentTab, 'LogOut', logout)}</View>
      </View>
      {
        //over lay view
      }
      <Animated.View
        style={{
          flexGrow: 1,
          backgroundColor: 'white',
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          paddingHorizontal: 15,
          paddingVertical: showMenu ? 40 : 20,
          borderRadius: showMenu ? 15 : 0,
          //transform view ..
          transform: [{scale: scaleValue}, {translateX: offsetValue}],
        }}>
        {
          //menu button
        }
        <Animated.View
          style={{
            transform: [{translateY: closeButtonOffset}],
          }}>
          <TouchableOpacity
            onPress={() => {
              //action here ...
              //scalling the view ..
              Animated.timing(scaleValue, {
                toValue: showMenu ? 1 : 0.88,
                duration: 300,
                useNativeDriver: true,
              }).start();

              //translate X
              Animated.timing(offsetValue, {
                toValue: showMenu ? 0 : 220,
                duration: 300,
                useNativeDriver: true,
              }).start();

              //close button
              Animated.timing(closeButtonOffset, {
                toValue: !showMenu ? -30 : 0,
                duration: 300,
                useNativeDriver: true,
              }).start();

              setShowMenu(!showMenu);
            }}>
            <Image
              source={showMenu ? close : menu}
              style={{width: 20, height: 20, tintColor: 'black'}}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 30,
              fontWeight: 'bold',
              color: 'black',
              paddingTop: 20,
            }}>
            {currentTab}
          </Text>
          <Image
            source={profile}
            style={{
              width: '100%',
              height: 300,
              borderRadius: 15,
              marginTop: 20,
            }}
          />
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              paddingTop: 15,
              color: 'black',
              paddingBottom: 5,
            }}>
            Agus Sulam
          </Text>
          <Text style={{}}>
            Youtuber, Designer, FrontEnd Dev, FullStack Dev
          </Text>
        </Animated.View>
      </Animated.View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#D9435E',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});
