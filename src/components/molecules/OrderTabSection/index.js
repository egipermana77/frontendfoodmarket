import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import ItemListFood from '../ItemListFood';
import {FoodDummy1, FoodDummy2, FoodDummy3, FoodDummy4} from '../../../assets';
import {useNavigation} from '@react-navigation/native';

const renderTabBar = props => (
  <TabBar
    {...props}
    indicatorStyle={{
      backgroundColor: '#020202',
      height: 3,
    }}
    style={{
      backgroundColor: 'white',
      elevation: 0,
      shadowOpacity: 0,
      borderBottomColor: '#F2F2F2',
      borderBottomWidth: 1,
    }}
    tabStyle={{width: 'auto'}}
    renderLabel={({route, focused, color}) => (
      <Text
        style={{
          fontFamily: 'Poppins-Medium',
          color: focused ? '#020202' : '#8092A3',
        }}>
        {route.title}
      </Text>
    )}
  />
);

const InProgress = () => {
  const navigation = useNavigation();
  return (
    <ScrollView>
      <View style={{paddingTop: 8, paddingHorizontal: 24}}>
        <ItemListFood
          image={FoodDummy1}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="in-progress"
          onPress={() => navigation.navigate('OrderDetail')}
        />
        <ItemListFood
          image={FoodDummy2}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="in-progress"
          onPress={() => navigation.navigate('OrderDetail')}
        />
        <ItemListFood
          image={FoodDummy3}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="in-progress"
          onPress={() => navigation.navigate('OrderDetail')}
        />
        <ItemListFood
          image={FoodDummy4}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="in-progress"
          onPress={() => navigation.navigate('OrderDetail')}
        />
        <ItemListFood
          image={FoodDummy3}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="in-progress"
          onPress={() => navigation.navigate('OrderDetail')}
        />
      </View>
    </ScrollView>
  );
};

const PastOrder = () => {
  const navigation = useNavigation();
  return (
    <ScrollView>
      <View style={{paddingTop: 8, paddingHorizontal: 24}}>
        <ItemListFood
          image={FoodDummy4}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="past-orders"
          date="Jan 12, 14:00"
          status="success"
          onPress={() => navigation.navigate('OrderDetail')}
        />
        <ItemListFood
          image={FoodDummy2}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="past-orders"
          date="Jan 12, 14:00"
          status="success"
          onPress={() => navigation.navigate('FoodDetail')}
        />
        <ItemListFood
          image={FoodDummy4}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="past-orders"
          date="Jan 12, 14:00"
          onPress={() => navigation.navigate('FoodDetail')}
        />
        <ItemListFood
          image={FoodDummy1}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="past-orders"
          date="Jan 12, 14:00"
          status="cancel"
          onPress={() => navigation.navigate('FoodDetail')}
        />
        <ItemListFood
          image={FoodDummy3}
          name="Sop Iga"
          items={3}
          price="50.000"
          type="past-orders"
          date="Jan 12, 14:00"
          status="success"
          onPress={() => navigation.navigate('FoodDetail')}
        />
      </View>
    </ScrollView>
  );
};

const renderScene = SceneMap({
  1: InProgress,
  2: PastOrder,
});

const OrderTabSection = () => {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '1', title: 'In Progress'},
    {key: '2', title: 'Past Orders'},
  ]);
  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      style={{backgroundColor: 'white'}}
    />
  );
};

export default OrderTabSection;

const styles = StyleSheet.create({});
