import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Button, Gap, Headers, ItemListFood, ItemValue} from '../../components';
import {FoodDummy4, IcBackWhite} from '../../assets';

const OrderDetail = ({navigation}) => {
  return (
    <ScrollView>
      <Headers
        title="Payment"
        subTitle="Payment Detail"
        onBack={() => {
          navigation.goBack();
        }}
      />
      <View style={styles.content}>
        <Text style={styles.label}>Item Ordered</Text>
        <ItemListFood
          image={FoodDummy4}
          type="order-summary"
          name="Sop Iga"
          price="34.000"
          items={14}
        />
        <Text style={styles.label}>Details Transaction</Text>
        <ItemValue label="Soup Bumil" value="IDR. 34.000" />
        <ItemValue label="Driver" value="IDR. 8.000" />
        <ItemValue label="Tax 10%" value="IDR. 3.400" valueColor="red" />
        <ItemValue
          label="Total Price"
          value="IDR. 45.400"
          valueColor="#1ABC9C"
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Delivery To :</Text>
        <ItemValue label="Name" value="Egi Permana" />
        <ItemValue label="Address" value="Cikutra" />
        <ItemValue label="Phone No" value="082321596683" />
        <ItemValue label="House No" value="11" />
        <ItemValue label="City" value="Bandung" />
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Order Status :</Text>
        <ItemValue label="#FM90834S" value="PAID" valueColor="#1ABC9C" />
      </View>
      <View style={styles.button}>
        <Button
          title="Cancel My Order"
          color="#D9435E"
          textColor="white"
          onPress={() => navigation.replace('SuccessOrder')}
        />
      </View>
      <Gap height={40} />
    </ScrollView>
  );
};

export default OrderDetail;

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 16,
    marginTop: 24,
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    marginBottom: 8,
  },
  button: {paddingHorizontal: 24, marginTop: 24},
});
