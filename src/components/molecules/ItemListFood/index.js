import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Rating from '../Rating';

/**
 *
 * @param {type untuk kondisi } param0
 * @returns
 * 1. product
 * 2. order-summary
 * 3. in-progress
 * 4. past-orders
 */

const ItemListFood = ({
  image,
  onPress,
  rating,
  items,
  price,
  name,
  date,
  status,
  type,
}) => {
  const renderContent = () => {
    switch (type) {
      case 'product':
        //item list products seperti di home page
        return (
          <>
            <View style={styles.txtContainer}>
              <Text style={styles.title}>{name}</Text>
              <Text style={styles.price}>IDR. {price}</Text>
            </View>
            <Rating rating={rating} />
          </>
        );
      case 'order-summary':
        //item order summary
        return (
          <>
            <View style={styles.txtContainer}>
              <Text style={styles.title}>{name}</Text>
              <Text style={styles.price}>IDR. {price}</Text>
            </View>
            <Text style={styles.items}>{items} items</Text>
          </>
        );
      case 'in-progress':
        //item in progress
        return (
          <>
            <View style={styles.txtContainer}>
              <Text style={styles.title}>{name}</Text>
              <Text style={styles.price}>
                {items} items . IDR {price}
              </Text>
            </View>
          </>
        );
      case 'past-orders':
        return (
          <>
            <View style={styles.txtContainer}>
              <Text style={styles.title}>{name}</Text>
              <Text style={styles.price}>
                {items} items . IDR {price}
              </Text>
            </View>
            <View>
              <Text style={styles.date}>{date}</Text>
              <Text style={styles.status}>{status}</Text>
            </View>
          </>
        );
      //item past orders
      default:
        //item list products
        return (
          <>
            <View style={styles.txtContainer}>
              <Text style={styles.title}>{name}</Text>
              <Text style={styles.price}>IDR. {price}</Text>
            </View>
            <Rating />
          </>
        );
    }
  };

  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.container}>
        <Image source={image} style={styles.img} />
        {renderContent()}
      </View>
    </TouchableOpacity>
  );
};

export default ItemListFood;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: 8,
    alignItems: 'center',
  },
  img: {
    width: 60,
    height: 60,
    borderRadius: 8,
    overflow: 'hidden',
    marginRight: 12,
  },
  txtContainer: {flex: 1},
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
  },
  price: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
    color: '#8092A3',
  },
  items: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
  },
  date: {
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
  },
  status: {
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    color: '#D9435E',
  },
});
