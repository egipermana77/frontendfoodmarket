import {showMessage} from 'react-native-flash-message';
const showToast = (message, type) => {
  showMessage({
    message: message,
    type: type === 'success' ? 'success' : 'danger',
    backgroundColor: type === 'success' ? '#1ABC9C' : '#D9435E',
  });
};

export default showToast;
